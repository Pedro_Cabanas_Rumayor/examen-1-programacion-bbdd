package com.example.examen1_programacinbbdd.ui.profile

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.examen1_programacinbbdd.R
import com.example.examen1_programacinbbdd.models.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class ProfileFragment : Fragment() {

    lateinit var userProfile: Profile

    //TAG
    var TAG: String = "ProfileFragment"

    //firebase
    private lateinit var auth: FirebaseAuth

    //firestore
    val db = Firebase.firestore

    //textView
    lateinit var tvEmail: TextView
    lateinit var tvName: TextView
    lateinit var tvBirthday: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val currentUserUid = auth.currentUser!!.uid
        getUserProfile(currentUserUid)
        //Register view.findViewById
        tvEmail = view.findViewById(R.id.tvEmail)
        tvName = view.findViewById(R.id.tvName)
        tvBirthday = view.findViewById(R.id.tvBirthday)
    }

    fun getUserProfile(uid: String) {
        val docRef = db.collection("profiles").document(uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                    userProfile = document.toObject(Profile::class.java)!!
                    tvEmail.text = userProfile.email
                    tvBirthday.text = userProfile.birthday
                    tvName.text = userProfile.name
                } else {
                    Log.d(TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }
    }
}