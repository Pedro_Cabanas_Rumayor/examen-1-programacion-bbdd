package com.example.examen1_programacinbbdd.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.examen1_programacinbbdd.R
import com.example.examen1_programacinbbdd.models.Note
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class HomeFragment : Fragment() {

    //TAG
    var TAG: String = "HomeFragment"

    //Recycler view
    lateinit var rvNotes: RecyclerView


    //FireBase
    private lateinit var auth: FirebaseAuth


    //FireStore
    val db = Firebase.firestore
    var notesList= arrayListOf<Note>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Recycler view.findViewById
        rvNotes = view.findViewById(R.id.rvNotes)
        //Recycler layoutManager
        rvNotes.layoutManager = LinearLayoutManager(requireContext())

        val currentUserUid = auth.currentUser!!.uid
        getUserNotesFromFireStore(currentUserUid)
    }

    fun getUserNotesFromFireStore(uid: String) {
        db.collection("profiles").document(uid)
            .collection("notes")
            .get()
            .addOnSuccessListener { documents ->
                Log.d(TAG, "Notes correctly downloaded")
                notesList.addAll(documents.toObjects(Note::class.java))
                rvNotes.adapter = NotesListAdapter(notesList, requireContext())
                println(notesList)
            }
            .addOnFailureListener { e -> Log.w(TAG, "Error notes downloaded", e) }
    }
}