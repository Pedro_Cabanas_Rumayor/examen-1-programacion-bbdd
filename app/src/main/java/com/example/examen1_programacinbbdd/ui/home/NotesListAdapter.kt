package com.example.examen1_programacinbbdd.ui.home

import android.content.Context
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.examen1_programacinbbdd.R
import com.example.examen1_programacinbbdd.models.Note

class NotesListAdapter(var notesList: ArrayList<Note>, val context: Context) :
    RecyclerView.Adapter<NoteCell>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteCell { //Elemento que pinta N veces
        var inflater: LayoutInflater = LayoutInflater.from(context)
        val note_profile: View = inflater.inflate(R.layout.cell_note, parent, false)
        return NoteCell(note_profile)
    }

    override fun onBindViewHolder(holder: NoteCell, position: Int) { //Escoge el elemento de la lista que va a pintar cada vez
        holder.asignValues(notesList[position])

    }

    override fun getItemCount(): Int { //Tamaño de lista
        return notesList.size
    }
}