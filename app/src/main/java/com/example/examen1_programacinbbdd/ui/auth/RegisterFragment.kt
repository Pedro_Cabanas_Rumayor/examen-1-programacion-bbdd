package com.example.examen1_programacinbbdd.ui.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.fragment.findNavController
import com.example.examen1_programacinbbdd.MainActivity
import com.example.examen1_programacinbbdd.R
import com.example.examen1_programacinbbdd.models.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class RegisterFragment : Fragment(), View.OnClickListener {
    //TAG
    var TAG: String = "RegisterFragment"

    //Register view
    lateinit var etEmailRegister: EditText
    lateinit var etPasswordRegister: EditText
    lateinit var etConfirmPasswordRegister: EditText
    lateinit var etNameRegister: EditText
    lateinit var etBirthdayRegister: EditText
    lateinit var btnCancelRegister: Button
    lateinit var btnConfirmRegister: Button

    //FireBase
    private lateinit var auth: FirebaseAuth


    //FireStore
    val db = Firebase.firestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this authNavHost
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Register view.findViewById
        etEmailRegister = view.findViewById(R.id.etEmailRegister)
        etPasswordRegister = view.findViewById(R.id.etPasswordRegister)
        etConfirmPasswordRegister = view.findViewById(R.id.etConfirmPasswordRegister)
        etNameRegister = view.findViewById(R.id.etNameRegister)
        etBirthdayRegister = view.findViewById(R.id.etBirthdayRegister)
        btnCancelRegister = view.findViewById(R.id.btnCancelRegister)
        btnConfirmRegister = view.findViewById(R.id.btnConfirmRegister)
        //Register OnClickListener
        btnCancelRegister.setOnClickListener(this)
        btnConfirmRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            //onclicks de login authNavHost
            this.btnCancelRegister -> {
                findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
            }
            this.btnConfirmRegister -> {
                var emailRegister = etEmailRegister.text.toString()
                var passwordRegister = etPasswordRegister.text.toString()
                if ((emailRegister.isNotEmpty() and passwordRegister.isNotEmpty() and etConfirmPasswordRegister.text.toString()
                        .isNotEmpty() and etNameRegister.text.toString()
                        .isNotEmpty() and etBirthdayRegister.text.toString().isNotEmpty()
                            ) and (passwordRegister == etConfirmPasswordRegister.text.toString())
                ) {
                    registerUserOnFirebase(
                        emailRegister, passwordRegister
                    )
                } else {
                    Toast.makeText(
                        requireContext(), "Credentials cannot be empty or password are not equal",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    fun transictToMainActivity() {
        val intent: Intent = Intent(requireContext(), MainActivity::class.java)
        requireActivity().startActivity(intent)
        requireActivity().finish()
    }

    fun registerUserOnFirebase(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val currentUserUid = auth.currentUser!!.uid
                    addProfileToFireStore(
                        currentUserUid,
                        email,
                        etNameRegister.text.toString(),
                        etBirthdayRegister.text.toString()
                    )
                    transictToMainActivity()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        requireContext(), "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    fun addProfileToFireStore(id: String, email: String, name: String, birthday: String) {
        var newProfile: Profile = Profile(email, name, birthday)
        db.collection("profile").document(id)
            .set(newProfile)
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

    }
}