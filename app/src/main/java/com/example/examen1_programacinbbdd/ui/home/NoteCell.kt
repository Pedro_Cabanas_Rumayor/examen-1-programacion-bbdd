package com.example.examen1_programacinbbdd.ui.home

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.examen1_programacinbbdd.R
import com.example.examen1_programacinbbdd.models.Note

class NoteCell(var itemview: View):RecyclerView.ViewHolder(itemview) {
    fun asignValues(note: Note){
        val tvNoteTitle: TextView = itemView.findViewById(R.id.tvNoteTitle)
        val tvNoteContent: TextView = itemView.findViewById(R.id.tvNoteDescription)
        tvNoteTitle.text = note.title
        tvNoteContent.text = note.description
    }
}