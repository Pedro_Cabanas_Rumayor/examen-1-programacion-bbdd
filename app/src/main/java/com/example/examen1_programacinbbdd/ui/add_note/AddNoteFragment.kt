package com.example.examen1_programacinbbdd.ui.add_note

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.examen1_programacinbbdd.R
import com.example.examen1_programacinbbdd.models.Note
import com.example.examen1_programacinbbdd.models.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class AddNoteFragment : Fragment(), View.OnClickListener {

    //TAG
    var TAG: String = "RegisterFragment"

    //Note view
    lateinit var etDescrption: EditText
    lateinit var etTitle: EditText
    lateinit var btnSave: Button

    //FireBase
    private lateinit var auth: FirebaseAuth


    //FireStore
    val db = Firebase.firestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_add_note, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Note view.findViewById
        etDescrption = view.findViewById(R.id.etDescription)
        etTitle = view.findViewById(R.id.etDescription)
        btnSave = view.findViewById(R.id.btnSave)
        //Note OnClickListener
        btnSave.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            //onclicks
            this.btnSave -> {
                var description = etDescrption.text.toString()
                var title = etTitle.text.toString()
                val currentUserUid = auth.currentUser!!.uid
                if (title.isNotEmpty()) {
                    addNoteToFireStore(currentUserUid, title, description)
                }
            }
        }
    }

    fun addNoteToFireStore(uid: String, title: String, description: String) {
        var newNote: Note = Note(title, description)
        db.collection("profiles").document(uid)
            .collection("notes")
            .add(newNote)
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot successfully written!")
                Toast.makeText(
                    requireContext(), "Note created",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }
    }
}